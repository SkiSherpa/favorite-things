Welcome to the first Git exercise!
Completing these prompts will help you get more comfortable with
Git.

1. What is your favorite color?
Right now, purple
***
  Remember to:
    - add
    - commit
    - push
  your answer before answering the next question!
***

2. What is your favorite food?
  Mango Sticky Rice

3. Who is your favorite fictional character?
Currently, I guess Bakugo, also known as Great Explosion Murder God Dynamight

4. What is your favorite animal?
Noodle!!!! Which is my dog, who is a Corgi... But I also like Cheethas.

5. What is your favorite programming language? (Hint: You can always say Python!!)
JS, I know it better.
